//
//  ViewController.swift
//  test
//
//  Created by Deeksha Verma on 25/04/17.
//  Copyright © 2017 Deeksha Verma. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate
{
    var searchText: String! = ""
    @IBOutlet weak var peopleTableView: UITableView!
    var peopleArray : NSMutableArray!
    var page:NSInteger!
    var isLoadMore:Bool!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        peopleArray = NSMutableArray()
        peopleList()
        isLoadMore = false
        page = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Textfield Delegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        searchText = textField.text
        page = 0
        isLoadMore = false
        peopleList()
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        return true
    }
    
    //MARK: tableview Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if peopleArray.count>0
        {
            return peopleArray.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell:UITableViewCell?
        let cellIdentifier = "ListCell"
        
        cell=tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as UITableViewCell?
        
        if(cell==nil)
            
        {
            cell=UITableViewCell(style:UITableViewCellStyle.subtitle, reuseIdentifier: cellIdentifier)
        }
        if peopleArray.count>0
        {
            let image = cell?.viewWithTag(1) as! UIImageView
            if let _ = (peopleArray.object(at: indexPath.row) as AnyObject).object(forKey: "profile_img_link") as? String
            {
                let userImageLink = String(format: "http://104.215.251.233:81/d_MNetV2/public/images/%@", (peopleArray.object(at: indexPath.row) as AnyObject).object(forKey: "profile_img_link") as! String)
                
                image.setImageWith(NSURL(string: userImageLink)! as URL, placeholderImage: UIImage(named: ""))
            }
            
            
            let username = cell?.viewWithTag(2) as! UILabel
            if let _ = (peopleArray.object(at: indexPath.row) as AnyObject).object(forKey: "username") as? String
            {
                 username.text = (peopleArray.object(at: indexPath.row) as AnyObject).object(forKey: "username") as? String
            }
           
            
            let comp_name = cell?.viewWithTag(3) as! UILabel
            
            if let _ = (peopleArray.object(at: indexPath.row) as AnyObject).object(forKey: "comp_name") as? String
            {
            comp_name.text = (peopleArray.object(at: indexPath.row) as AnyObject).object(forKey: "comp_name") as? String
            }
            let block = cell?.viewWithTag(4) as! UILabel
//            block.text = "Block"
            if let _ = (peopleArray.object(at: indexPath.row) as AnyObject).object(forKey: "user_designation") as? String
            {
            block.text = (peopleArray.object(at: indexPath.row) as AnyObject).object(forKey: "user_designation") as? String
            }
            
            if indexPath.row == peopleArray.count-1 && isLoadMore
            {
                if peopleArray.count % 10 == 0
                {
                    isLoadMore = true
                    loadMore()
                }
            }
        }
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let detailVC: DetailViewController! = mainStoryboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        detailVC.dataDictionary = peopleArray.object(at: indexPath.row) as! NSDictionary
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    //MARK: Get list of People
    func peopleList()
    {
        let params = NSMutableDictionary()
        params.setValue("31", forKey: "user_id")
        
        params.setValue("1", forKey:"group_id")
        
        params.setValue("B2B User", forKey:"data_type_id")
        
        params.setValue(page, forKey:"start")
        
        params.setValue("10", forKey:"limit")
        params.setValue(searchText, forKey:"query")
        let url = "http://104.215.251.233:81/d_MNetV2Service/MnetV2WebService/findUsersfuncTo"
        let manager = AFHTTPSessionManager()
        manager.post(url, parameters: params, progress: nil, success: {
            (operation, responseObject) in
            print(responseObject)
            
            let json = responseObject as? NSDictionary
            let jsonArray = json!.object(forKey: "status") as? NSArray
            if self.page == 0
            {
                self.peopleArray.removeAllObjects()
            }
            if jsonArray != nil
            {
                if jsonArray!.count > 0
                {
                    self.peopleArray.addObjects(from: jsonArray as! [AnyObject])
                    
                }
                if (jsonArray!.count > 9)
                {
                    self.isLoadMore = true
                }
                else
                {
                    self.isLoadMore = false
                }
            }
            else
            {
                self.isLoadMore = false
            }
            self.peopleTableView.reloadData()
            
            }, failure:
            {
                (operation, error) in
                print("Error: " + error.localizedDescription)
        })
    }
    
    
    /**
     *  load more data
     */
    func loadMore()
    {
        
        page = peopleArray.count/10
        if page == 0
        {
            
        }
        page = page + 1
        peopleList()
        
    }
    
}

