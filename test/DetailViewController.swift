//
//  DetailViewController.swift
//  test
//
//  Created by Deeksha Verma on 25/04/17.
//  Copyright © 2017 Deeksha Verma. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var dataDictionary : NSDictionary!
    
    @IBOutlet weak var header: UILabel!
    
    @IBOutlet weak var detailTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if let a = dataDictionary.object(forKey: "username") as? String
        {
            header.text = a
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //   MARK: TableView Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 230
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataDictionary.count>0
        {
            return 1
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell:UITableViewCell?
        let cellIdentifier = "ListCell"
        
        cell=tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as UITableViewCell?
        
        if(cell==nil)
            
        {
            cell=UITableViewCell(style:UITableViewCellStyle.subtitle, reuseIdentifier: cellIdentifier)
        }
        
        if dataDictionary.count>0
        {
            let image = cell?.viewWithTag(1) as! UIImageView
            image.layer.borderColor = UIColor.lightGray.cgColor
            image.layer.borderWidth = 0.5
            if let _ = dataDictionary.object(forKey: "profile_img_link") as? String
            {
                let userImageLink = String(format: "http://104.215.251.233:81/d_MNetV2/public/images/%@", (dataDictionary.object(forKey: "profile_img_link") as! String))
                
                image.setImageWith(NSURL(string: userImageLink)! as URL, placeholderImage: UIImage(named: ""))
            }
            
            
            let comp_name = cell?.viewWithTag(2) as! UILabel
            if let _ = dataDictionary.object(forKey: "comp_name") as? String
            {
                
                comp_name.text = dataDictionary.object(forKey: "comp_name") as? String
                
            }
            
            let branch_name = cell?.viewWithTag(3) as! UILabel
            if let _ = dataDictionary.object(forKey: "branch_name") as? String
            {
                branch_name.text = dataDictionary.object(forKey: "branch_name") as? String
            }
            let user_email = cell?.viewWithTag(4) as! UILabel
            if let _ = dataDictionary.object(forKey: "user_email") as? String
            {
                user_email.text = dataDictionary.object(forKey: "user_email") as? String
            }
            
        }
        cell?.selectionStyle = .none
        return cell!
    }
    //MARK: Back btn Action
    @IBAction func back(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
